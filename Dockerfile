#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/esb/src
COPY pom.xml /home/esb
RUN mvn -f /home/esb/pom.xml clean package -Dmaven.test.skip=true

#
# Package stage
#
FROM openjdk:8-jre-slim
COPY --from=build /home/esb/target /usr/local/lib/esb
EXPOSE 8082
ENTRYPOINT ["java","-jar","/usr/local/lib/esb/esb-0.1.jar"]