package com.thume.sigo.esb.Controller;

import com.thume.sigo.esb.Bean.SalesReportBean;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class SalesReportController extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        
        rest("/salesReport/{year}/{month}")                
                .get()
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route()
                .bean(SalesReportBean.class, "getSalesReport")
                .marshal().json(JsonLibrary.Jackson)
                .endRest();
        
    }
    
}
