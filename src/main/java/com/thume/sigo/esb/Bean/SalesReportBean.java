package com.thume.sigo.esb.Bean;

import fakereports.sigo.thume.SalesReportPort;
import fakereports.sigo.thume.SalesReportPortService;
import fakereports.sigo.thume.GetSalesReportRequest;
import fakereports.sigo.thume.GetSalesReportResponse;
import fakereports.sigo.thume.Sales;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;

public class SalesReportBean {

    private Logger logger = LoggerFactory.getLogger(SalesReportBean.class);
    private static SalesReportPort salesReport;

    @Autowired
    private Environment env;

    public SalesReportBean() {

    }

    public List<Sales> getSalesReport(@Header("year") int year, @Header("month") int month) {

        if (salesReport == null) {
            String salesReportEndPoint = env.getProperty("salesReport.endPoint");
            try {
                SalesReportPortService service = new SalesReportPortService(new URL(salesReportEndPoint));
                salesReport = service.getSalesReportPortSoap11();
            } catch (MalformedURLException ex) {
                logger.error(null, ex);
            }
        }

        logger.info(String.format("SalesReportRequest = Year:%d, Month:%d", year, month));
        GetSalesReportRequest req = new GetSalesReportRequest();
        req.setMonth(month);
        req.setYear(year);
        GetSalesReportResponse res = salesReport.getSalesReport(req);
        return res.getSales();
    }

}
